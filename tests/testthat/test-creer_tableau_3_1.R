test_that("creer_tableau_3_1 fonctionne", {
  indicateurs_rpls_illustrations <- lire_rpls_exemple() %>%
    dplyr::filter(Zone_ref)
  
  tab <- creer_tableau_3_1(data = indicateurs_rpls_illustrations, annee = 2019)
  testthat::expect_equal(attr(tab,'format'),'html')
})