test_that("get_id_reg fonctionne", {
  testthat::expect_is(get_id_reg("Bretagne"), "character")
  testthat::expect_equal(nchar(get_id_reg("53 Bretagne")), 2)
})

test_that("get_nom_reg fonctionne", {
  testthat::expect_is(get_nom_reg("53 Bretagne"), "character")
})
