# propre.rpls 0.4.1

* Uniformisation du **formatage des chiffres** (suppression du . anglosaxon pour les décimales, maintien d'un zéro après la virgule si la précision est d'un chiffre apès la virgule)
* Amélioration du **graphique 4.1 sur les DPE** : précision dans le champ couvert (DPE renseigné au lieu de DPE réalisé) et ajout d'un fond bland derrière les étiquettes
* Correction **calcul de la densité** de logements sociaux et du nombre de résidences principales
* Correction ligne avec DEPCOM à vide sur tab_result en 2016
* Modification de la numérotation des tableaux (Tableau au lieu de table)
* Correction du calcul du **nb de logements du parc récent**

# propre.rpls 0.4.0

* Amélioration du graphique 4.1 sur la performance énergétique du parc : les couleurs correspondents aux couleurs usuelles pour les DPE
* Correction verbatim 4 : simplification de l'intertitre et modification du dénominateur dans le calcul des pourcentages  
* Correction des indicateurs liés aux nombre de résidence principales (densités, nombre de pièces des RP)  
* Correction et précision du verbatim 6 sur le calcul des rangs : le rang est bien calculé et donne le rang en fonction des régions les plus chers  
* Correction du calcul de l'évolution du nombre de logements sociaux N/N-1 pour le tableau du chap 1  
* Correction du calcul des sorties du parc sur le tableau 3.1  
* Correction du calcul des âges moyens, du nombre de logements couverts par un DPE
* Intégration de COGiter 0.0.5 pour rpls_exemple
* suppression des lignes des arrondissements de Paris, Lyon et Marseille qui donnait le double des données pour ces villes
* Création d'une fonction `creer_carte()` qui permet de générer une carte sur tout indicateur de RPLS
* Création d'une fonction `fond_carto()` qui permet de générer une liste des fonds de carte nécessaires pour creer_carte()
* Suppression de fond_carto_dep() et remplacement de `fond_carto_epci()` par un alias sur `fond_carto()`
* Amélioration visuelle des cartes produites
* Modification du template avec ces nouvelles cartes. Cela produit 4 changements sur les pages Index.Rmd pour la création du fond de carte, et sur 01-evol_parc.Rmd, 05-tensions.Rmd et 06-loyers_financmts.Rmd pour l'appel des cartes.
* Modification du template au niveau du verbatim du chap 6 : modification des arguments data (`indicateurs_rpls` au lieu de `indicateurs_rpls_ref` et annee (`annee` au lieu de `2019`)
* Ajout de filtres de NA pour LOYERPRINC ET SURFHAB dans le calcul des loyers moyens (calculs des sommes des loyers et surfaces)
* Correction du calcul des nombres de pièces et et de la part de DPE du parc récent
* Correction du calcul nombre de sorties pour autre motif 
* Correction de la part de DPE réalisé


# propre.rpls 0.3.0  

* Ajout des données 2020
* Ajout des indicateurs complémentaires
* Structuration de la [page reference](https://rdes_dreal.gitlab.io/propre.rpls/reference/index.html) du package
* Mise à jour du theme du site du package
* Assemblage des vignettes par chapitre
* Reprise de la numérotation de la publication
* Ajout de fonctions de création des verbatims :  
  - `creer_verbatim_3()`
  - `creer_verbatim_4()`
  - `creer_verbatim_6()`
  - `creer_verbatim_mentions_legales()`
* Ajout du paramètre `epci` aux fonctions de création des tableaux pour afficher ou non des lignes EPCI
* Mise à jour du Guide de prise en main
* Passage au template bookdown de gouvdown
* Ajout d'un module shiny de consultation et d'export des données complémentaires, accessible par la fonction `run_rpls_explorer()`  
* Ajout du corpus de mentions légales
* Amélioration du verbatim du chap 1 pour mieux gérer les cas où le parc social recule

# propre.rpls 0.2.0

* Rédaction de la vignette Guide de prise en main.
* Peuplement du squelette bookdown avec les fonctions de création des illustrations.
* Ajout de la charte graphique Etat via {gouvdown}.
* Ajout de fonctions de création des illustrations :  

  - `creer_graphe_1_1()`  
  - `creer_tableau_1_1()`  
  - `creer_carte_1_1()`  
  - `creer_graphe_2_1()`  
  - `creer_graphe_3_1()`  
  - `creer_graphe_3_2()`  
  - `creer_tableau_3_1()`  
  - `creer_tableau_4_1()`  
  - `creer_graphe_4_1()`  
  - `creer_tableau_5_1()`  
  - `creer_carte_5_1()`  
  - `creer_carte_5_2()`  
  - `creer_graphe_6_1()`  
  - `creer_graphe_6_2()`  
  - `creer_carte_6_1()`  

* Ajout de la fonction `discretiser()` pour discrétiser les variables numériques pour les cartes.
* Ajout des fonctions `format_fr_nb` et `format_fr_pct` pour mettre en forme les chiffres et les pourcentages dans la publication.
* Ajout de la fonction `lire_rpls_exemple()` pour récupérer un jeu d'exemple.
* Ajout de la fonction `caption()` pour uniformiser la mention des sources dans les illustrations.
* Mise à jour des données du RP (passage à 2017).
* Ajout de fonctions de création des verbatims :  

  - `creer_verbatim_1()`
  - `creer_verbatim_2()`
  - `creer_verbatim_5()`


# propre.rpls 0.1.1

* Ajout du site web pkgdown pour les branches master et de dév du projet.  
* Ajout de l'indication code coverage pour le projet.  
* Correction d'un bug sur la doc.   

# propre.rpls 0.1.0

* Ajout des vignettes d'explication de la data-préparation et de mode d'emploi du modèle de publication.  
* Ajout des fonctions de data-préparation `dataprep()`, `fond_carto_epci()`, `fond_carto_dep()`.  
* Ajout des jeux de données `tab_result` and `lgt_rp`.   
* Ajout du squelette de la publication bookdown.  
* Ajout du fichier `NEWS.md` pour suivre les évolutions du package.  
