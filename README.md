
<!-- README.md is generated from README.Rmd. Please edit that file -->

# propre.rpls <img src='man/figures/logo.png' align="right" height="139" />

dev: [![pipeline
status](https://gitlab.com/rdes_dreal/propre.rpls/badges/dev/pipeline.svg)](https://gitlab.com/rdes_dreal/propre.rpls/-/commits/dev)[![coverage
report](https://gitlab.com/rdes_dreal/propre.rpls/badges/dev/coverage.svg)](https://gitlab.com/rdes_dreal/propre.rpls/-/commits/dev)

Le site web de présentation du package est :

  - pour la version validée (master) :
    <https://rdes_dreal.gitlab.io/propre.rpls/index.html>  
  - pour la version en développement (dev) :
    <https://rdes_dreal.gitlab.io/propre.rpls/dev/index.html>  
  - Le code coverage (commun master/dev) :
    <https://rdes_dreal.gitlab.io/propre.rpls/coverage.html>

<!-- badges: start -->

<!-- badges: end -->

Le but de {propre.rpls} est de faciliter la production de publications
régionales sur le parc locatif social à partir de la source RPLS. Il
contient un template bookdown paramétrable selon la région d’observation
et les fonctions nécessaires à la création des commentaires et
illustrations.

Vous pouvez installer propre.rpls depuis gitlab avec :

``` r
remotes::install_gitlab("rdes_dreal/propre.rpls")
```

## Utilisation

  - Créez un nouveau projet Rstudio, dans un nouveau répertoire et
    sélectionner `Publication RPLS` comme type de projet ;

  - Sélectionner vos paramètres (millésime, région, EPCIs);

  - Complétez les champs auteurs et date dans index.Rmd ;

  - Complétez éventuellement la liste des epci de référence sur lesquels
    vous voulez un zoom dans les illustrations ;

  - Lancez la compilation du bookdown ;

  - Intégrez vos commentaires et analyses ;-)

Plus de détail sur la prise en main du package au niveau de la vignette
[“Prise en
main”](https://rdes_dreal.gitlab.io/propre.rpls/articles/aa-prise-en-main.html)
