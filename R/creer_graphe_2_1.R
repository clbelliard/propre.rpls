#' Creation du graphique du chapitre sur les caracteristiques du parc, representant la repartition regionale des logements sociaux selon le nb de pieces, en distinguant le parc total du parc récent
#'
#' @description Mise en page du diagramme en bâtons représentant la répartition régionale de l’ensemble des logements sociaux selon le nb de pièces, pour le parc total et le parc récent.
#' @param data La table d'indicateurs préparée par dataprep() selon les inputs de l'utilisateur et filtrée sur le booléen Zone_ref.
#' @param annee Le millesime renseigné par l'utilisateur, au format numérique.
#' @param palette choix de la palette de couleur parmi celle de \code{gouvdown::\link{scale_color_gouv_discrete}}
#'
#' @return un ggplot intéractif
#'
#' @importFrom dplyr filter select starts_with mutate group_by ungroup
#' @importFrom forcats fct_relevel
#' @importFrom ggiraph geom_bar_interactive ggiraph
#' @importFrom ggplot2 ggplot aes position_dodge scale_x_discrete scale_y_continuous theme element_blank labs
#' @importFrom gouvdown scale_fill_gouv_discrete
#' @importFrom glue glue
#' @importFrom tidyr pivot_longer
#' @importFrom scales percent
#' @importFrom rlang .data
#' @export
#'
#' @examples
#' indicateurs_rpls_illustrations <- lire_rpls_exemple() %>%
#'  dplyr::filter(Zone_ref)
#'
#' creer_graphe_2_1(data = indicateurs_rpls_illustrations, annee = 2019)

creer_graphe_2_1 <- function(data, annee, palette = "pal_gouv_qual2") {
  creer_graphe_2_1 <- data %>%
    # filtre sur la région et pour le millesime souhaite
    dplyr::filter(.data$TypeZone == "R\u00e9gions" & .data$millesime == annee) %>%
    # selection des variables necessaires au graphe
    dplyr::select(.data$Zone, dplyr::starts_with("nb_piece")) %>%
    # passage au format long de la table en distinguant le nb de piece du type de logement (recent ou pas)
    tidyr::pivot_longer(
      cols = -.data$Zone, names_to = c("nb_piece", "type"), values_to = "values",
      names_pattern = "(nb_piece_[0-9]_plus|nb_piece_[0-9])[_]{0,1}(.*)"
    ) %>%
    # creation de la modalite parc total
    dplyr::mutate(type = ifelse(.data$type == "", "total", .data$type)) %>%
    dplyr::group_by(.data$type) %>%
    dplyr::mutate(freq = (.data$values / sum(.data$values)) * 100) %>%
    dplyr::ungroup()

  g_bar <- ggplot2::ggplot(data = creer_graphe_2_1, ggplot2::aes(
    x = .data$nb_piece,
    y = .data$freq,
    fill = forcats::fct_relevel(.data$type, "total", "recent"),
    # information a faire apparaitre dans les bulles: le nb_ls avec separateur des milliers
    tooltip = format_fr_pct(.data$freq)
  )) +
    # geom_bar en version interactif
    ggiraph::geom_bar_interactive(stat = "identity", position = ggplot2::position_dodge()) +
    # gestion des libelles de l axe des abscisses
    ggplot2::scale_x_discrete(
      breaks = c("nb_piece_1", "nb_piece_2", "nb_piece_3", "nb_piece_4", "nb_piece_5_plus"),
      labels = c("1 pi\u00e8ce", "2 pi\u00e8ces", "3 pi\u00e8ces", "4 pi\u00e8ces", "5 pi\u00e8ces et plus")
    ) +
    # gestion des libelles de la legende
    gouvdown::scale_fill_gouv_discrete(palette = palette, breaks = c("total", "recent"),
                                       labels = c("Parc total", "Parc r\u00e9cent (5 ans ou moins)")) +
    # formatage de l axe des ordonnees
    ggplot2::scale_y_continuous(labels = format_fr_nb) +
    ggplot2::theme(
      legend.title = ggplot2::element_blank(),
      legend.position = "bottom"
    ) +
    # habillage simple
    ggplot2::labs(
      title = glue::glue("R\u00e9partition r\u00e9gionale des logements sociaux \nselon le nombre de pi\u00e8ces au 01/01/{annee}"),
      subtitle = "Unit\u00e9 : %",
      x = "",
      y = "",
      caption = caption(sources = 1, mil_rpls = annee)
    )

  # transformation par ggiraph
  ggiraph::ggiraph(code = print(g_bar))
}
