---
title: "zz- Creation du projet de publication"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{zz- Creation du projet de publication}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```


Après avoir installé le package `{propre.rpls}`, l'utilisateur crée son projet RStudio de publication en sélectionnant :  
New projet > New directory > Projet type : Publication RPLS 
Une interface de sélection des paramètres s'ouvre alors :
![](img/ui_param.PNG)   

L'utilisateur y définit son choix de répertoire de travail (`path`), le millésime des données RPLS pour lesquels il souhaite réaliser une publication, la région observée et le type d'analyses par EPCI.

Cette interface est définie par le fichier propre.rpls/inst/rstudio/templates/project/skeleton.dcf.
Ces choix de paramètres sont utilisés par les fonctions du fichier `R/skeleton.R`, élaboré grâce à la [documentation de Rstudio sur les projet de template](https://rstudio.github.io/rstudio-extensions/rstudio_project_templates.html).

```{r setup}
library(propre.rpls)
```

## Fonction propre.rpls_file()

Cette fonction renvoie les adresses absolues des fichiers présents dans le répertoire d'installation du package à partir de leur adresse relative.
Exemple : 

```{r fun1}
propre.rpls_file("rstudio", "templates", "project", "ressources")
```

## Fonction propre.rpls_skeleton()

Fonction de création du répertoire du projet de l'utilisateur : à partir de la saisie des paramètres par l'utilisateur dans l'interface RStudio, cette fonction peuple le répertoire de travail de l'utilisateur avec le bookdown paramétré et fixe les paramètres dans l'entête du fichier `index.Rmd`
Elle renvoie `TRUE`. Exemple : 

```{r fun2, eval=FALSE}
propre.rpls_skeleton("rpls2019bretagne",
  annee = "2019", epci_ref = "1- Tous les EPCI de la zone",
  nom_region = "Bretagne"
)
```

Cette fonction va créer un projet RStudio pour la publication RPLS 2019 de la Bretagne, paramétrée pour détailler les tableaux et graphiques avec les données de tous les EPCI de la région, dans un répertoire intitulé "rpls2019bretagne".

